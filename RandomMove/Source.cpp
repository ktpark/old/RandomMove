#include <iostream>
#include <windows.h>
#include <thread>
#include <string>

bool isRunning = true;

void gear()
{
	INPUT ip;

	// Set up a generic keyboard event.
	ip.type = INPUT_KEYBOARD;
	ip.ki.wScan = 0; // hardware scan code for key
	ip.ki.time = 0;
	ip.ki.dwExtraInfo = 0;

	while (isRunning)
	{
		std::string msg = "Gear: ";
		int num = rand() % 3;
		int delay = rand() % 900 + 100;
		int delay2 = rand() % 100 + 100;

		int key;
		switch (num)
		{
		case 0:
			msg += "Up ";
			key = 'W';
			break;
		case 1:
			msg += "Down ";
			key = 'S';
			break;
		case 2:
			msg += "Pass " + std::to_string(delay) + "ms";
			std::cout << msg << std::endl;
			continue;
		}

		msg += std::to_string(delay) + "ms, " + std::to_string(delay2) + "ms";
		std::cout << msg << std::endl;

		// Press the key
		ip.ki.wVk = key;
		ip.ki.dwFlags = 0; // 0 for key press
		SendInput(1, &ip, sizeof(INPUT));

		Sleep(delay);

		// Release the key
		ip.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
		SendInput(1, &ip, sizeof(INPUT));

		Sleep(delay2);
	}
}

void turn()
{
	INPUT ip;

	// Set up a generic keyboard event.
	ip.type = INPUT_KEYBOARD;
	ip.ki.wScan = 0; // hardware scan code for key
	ip.ki.time = 0;
	ip.ki.dwExtraInfo = 0;
	
	while (isRunning)
	{
		std::string msg = "Turn: ";
		int num = rand() % 3;
		int delay = rand() % 200 + 200;
		int delay2 = rand() % 100 + 50;

		int key;
		switch (num)
		{
		case 0:
			msg += "Left ";
			key = 'A';
			break;
		case 1:
			msg += "Right ";
			key = 'D';
			break;
		case 2:
			msg += "Pass " + std::to_string(delay) + "ms";
			std::cout << msg << std::endl;
			continue;
		}

		msg += std::to_string(delay) + "ms, " + std::to_string(delay2) + "ms";
		std::cout << msg << std::endl;

		// Press the key
		ip.ki.wVk = key;
		ip.ki.dwFlags = 0; // 0 for key press
		SendInput(1, &ip, sizeof(INPUT));

		Sleep(delay);

		// Release the key
		ip.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
		SendInput(1, &ip, sizeof(INPUT));

		Sleep(delay2);
	}
}

void shift()
{
	INPUT ip;

	ip.type = INPUT_KEYBOARD;
	ip.ki.wScan = 0;
	ip.ki.time = 0;
	ip.ki.dwExtraInfo = 0;
	ip.ki.wVk = VK_SHIFT;

	while (isRunning)
	{
		int delay = rand() % 9000 + 1000;
		int delay2 = rand() % 2000 + 500;

		std::cout << "Shift: " << std::to_string(delay) + "ms, " + std::to_string(delay2) + "ms"  << std::endl;

		Sleep(delay);
		ip.ki.dwFlags = 0; // 0 for key press
		SendInput(1, &ip, sizeof(INPUT));

		Sleep(delay2);
		ip.ki.dwFlags = KEYEVENTF_KEYUP;
		SendInput(1, &ip, sizeof(INPUT));
	}
}

void mouse()
{
	INPUT ip;

	ip.type = INPUT_MOUSE;

	while (isRunning)
	{
		std::string msg = "Mouse: ";
		int num = rand() % 3;
		int delay = rand() % 250 + 250;
		int delay2 = rand() % 100 + 100;

		switch (num)
		{
		case 0:
			msg += "Boost ";
			delay2 += rand() % 500;
			ip.mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
			break;
		case 1:
			msg += "Jump ";
			ip.mi.dwFlags = MOUSEEVENTF_RIGHTDOWN;
			break;
		default:
			msg += "Pass " + std::to_string(delay) + "ms";
			std::cout << msg << std::endl;
			Sleep(delay);
			continue;
		}

		msg += std::to_string(delay) + "ms, " + std::to_string(delay2) + "ms";
		std::cout << msg << std::endl;

		Sleep(delay);
		SendInput(1, &ip, sizeof(INPUT));

		switch (num)
		{
		case 0:
			ip.mi.dwFlags = MOUSEEVENTF_LEFTUP;
			break;
		case 1:
			ip.mi.dwFlags = MOUSEEVENTF_RIGHTUP;
			break;
		}

		Sleep(delay2);
		SendInput(1, &ip, sizeof(INPUT));
	}
}

int main()
{

	for (int i = 10; i >= 0; i--)
	{
		Sleep(1000);
		std::cout << "Starting AFK Movements in " << i << std::endl;
	}

	std::thread gear(gear);
	std::thread turn(turn);
	std::thread shift(shift);
	std::thread mouse(mouse);

	std::cout << "Threads started..." << std::endl;

	while (!(GetAsyncKeyState(VK_HOME) & 0x8000))
		Sleep(10);

	isRunning = false;

	std::cout << "Waiting for threads to terminate..." << std::endl;

	gear.join();
	turn.join();
	shift.join();
	mouse.join();

	return 0;
}